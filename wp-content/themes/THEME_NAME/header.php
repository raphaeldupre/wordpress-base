<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php wp_title(); ?></title>
    <?php wp_head(); ?>

</head>
<body <?php body_class(); ?> <?php echo (is_front_page()) ? 'home' : 'not-home'; ?>>
    
    <!-- Menu header -->
    <!-- <nav class="navbar fixed-top navbar-expand-md navbar-light" role="navigation"> -->
    <nav class="navbar navbar-expand-lg navbar-light" role="navigation">
        <div class="container">
            <a class="blog-header-logo text-dark" href="<?php bloginfo('url'); ?>">
                <?php  echo '<img src="'.get_template_directory_uri().'/images/LOGO.png" alt="DESCRIPTION LOGO">'; ?>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header-menu" aria-controls="#header-menu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <?php
                wp_nav_menu(array(
                    'theme_location'    => 'menu_principal',
                    'depth'             => 2,
                    'container'         => 'div',
                    'container_class'   => 'collapse navbar-collapse',
                    'container_id'      => 'header-menu',
                    'menu_class'        => 'nav navbar-nav',
                    'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                    'walker'            => new WP_Bootstrap_Navwalker(),
                ));
            ?>
        </div>
    </nav>
    <!-- Fin du menu header -->

<?php wp_footer(); ?>